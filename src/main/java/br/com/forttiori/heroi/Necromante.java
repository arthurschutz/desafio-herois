package br.com.forttiori.heroi;


public class Necromante extends Personagem{

    private int inteligencia;

    public Necromante(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        super(name, vida, level, dinheiro, atack, defesa);
        this.inteligencia = 5;
    }

    public int getInteligencia() {
        return inteligencia;
    }

}
