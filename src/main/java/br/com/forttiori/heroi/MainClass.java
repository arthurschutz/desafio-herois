package br.com.forttiori.heroi;

import java.util.Scanner;

public class MainClass {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Guerreiro guerreiro = new Guerreiro("John", 100, 5, 100, 20, 10);
        guerreiro.addInventario("escudo");
        guerreiro.addInventario("espada");

        System.out.println();
        System.out.println("O GUERREIRO ESTÁ PRONTO PARA A BATALHA!");
        System.out.println(guerreiro.toString());
        System.out.println("Característica especial: Bravura = " + guerreiro.getBravura());
        System.out.println();


        Necromante necromante = new Necromante("Norman", 100, 5, 100, 10, 15);

        System.out.println();
        System.out.println("O NECROMANTE ESTÁ PRONTO PARA A BATALHA!");
        System.out.println(necromante.toString());
        System.out.println("Característica especial: Inteligencia = " + necromante.getInteligencia());
        System.out.println();

        System.out.println("INICIAR BATALHA!");

        guerreiro.caminnhar();
        guerreiro.atacar(necromante);
        necromante.defender(necromante);
        System.out.println("-------------");

        necromante.caminnhar();
        necromante.ataqueEspecial(guerreiro);
        guerreiro.defender(guerreiro);

        System.out.println();
        System.out.println("Situação da Batalha...");
        System.out.println(guerreiro.toString());
        System.out.println(necromante.toString());

        sc.close();

    }
}
