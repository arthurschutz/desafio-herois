package br.com.forttiori.heroi;

public abstract class Personagem {

    private String name;
    private int vida;
    private int level;
    private int dinheiro;
    private int atack;
    private int defesa;


    public Personagem(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        this.name = name;
        this.vida = vida;
        this.level = level;
        this.dinheiro = dinheiro;
        this.atack = atack;
        this.defesa = defesa;
    }

    public void atacar(Personagem oponente) {
        oponente.vida -= atack;
    }

    public void defender(Personagem defensor) {
        defensor.vida += defesa;
    }

    public String caminnhar() {
        return "Eu estou caminhando";
    }

    public void ataqueEspecial(Personagem oponente) {
        oponente.vida -= atack + 5;
    }

    @Override
    public String toString() {
        return "Personagem{" +
                "name='" + name + '\'' +
                ", vida=" + vida +
                ", level=" + level +
                ", dinheiro=" + dinheiro +
                ", atack=" + atack +
                ", defesa=" + defesa +
                '}';
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getDinheiro() {
        return dinheiro;
    }

    public void setDinheiro(int dinheiro) {
        this.dinheiro = dinheiro;
    }

    public int getAtack() {
        return atack;
    }

    public void setAtack(int atack) {
        this.atack = atack;
    }

    public int getDefesa() {
        return defesa;
    }

    public void setDefesa(int defesa) {
        this.defesa = defesa;
    }
}
