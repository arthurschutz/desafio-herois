package br.com.forttiori.heroi;

import java.util.ArrayList;
import java.util.List;

public class Guerreiro extends Personagem  {

    private final int bravura;
    private final List<String> inventario;

    public Guerreiro(String name, int vida, int level, int dinheiro, int atack, int defesa) {
        super(name, vida, level, dinheiro, atack, defesa);
        this.bravura = 5;
        this.inventario= new ArrayList<>();
    }

    public void addInventario(String item){
        this.inventario.add(item);

    }

    public int getBravura() {
        return bravura;
    }

}
